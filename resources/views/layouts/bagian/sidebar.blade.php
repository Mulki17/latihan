          <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                </span>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="{{ route('home')}}" class="active"><i class="fa fa-dashboard fa-fw"></i> Home</a>
                            </li>
                            <li>
                                <a href=""><i class="fa fa-users fa-fw"></i> Data Supplier</a>
                            </li>
                            <li>
                                <a href="{{ route('barang.index')}}"><i class="fa fa-table fa-fw"></i> Data Barang</a>
                            </li>
                            <li>
                                <a href="{{ route('barang_masuk.index')}}"><i class="fa fa-table fa-fw"></i> Data Barang Masuk</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-table fa-fw"></i> Data Barang Keluar</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-table fa-fw"></i> Data Pelanggan</a>
                            </li>
                            <li>
                                <a href="{{ route('transaksi.index')}}"><i class="fa fa-table fa-fw"></i> Transaksi</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-edit fa-fw"></i> Laporan Penjualan</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>