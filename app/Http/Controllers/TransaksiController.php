<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use session;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = Transaksi::All();
        return view('transaksi.index',compact('transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang = barang::all();
        return view('transaksi.create',compact('barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        

        // $barang = Barang::findOrFail($request->barang_id);
        // $barang->jumlah     -= $request->qty;
        // $barang->harga       = $request->harga;
        // $barang->total_harga = $request->total_harga;
        // $barang->save();

        // Session::flash('flash_message','transaksi added');

    

        $transaksi = new Transaksi;
        $transaksi->barang_id   = $request->barang_id;
        $transaksi->harga       = $request->harga;
        $transaksi->qty         = $request->qty;
        $transaksi->total_harga = $transaksi->harga * $transaksi->qty;
        $transaksi->save();

        // Pengurangan STOK Barang
         $barang = Barang::findOrFail($request->barang_id);
         $barang->jumlah -= $request->qty;
         $barang->save();
        
        return redirect()->route('transaksi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi $transaksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaksi $transaksi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = Transaksi::findOrFail($id);
        $transaksi->delete();

         $barang = Barang::findOrFail($barang->id);
         $barang->jumlah += $transaksi;
         $barang->save();
        return redirect()->route('transaksi.index');
    
    }

    // menentuka jumlah keseluruhan laporan TRANSAKSI
    public function laporan(){
        $transaksi = DB::table('users')
                        ->select('count(id) as laporan')
                        ->groupBy('id')
                        ->get();
        return view('home',compact('transaksi'));
    }
}
