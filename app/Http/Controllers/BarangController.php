<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::All();
        return view('barang.index', compact('barang'));
       
        //Ubah ke Json
        // return response()->json([
        //     'success' => true,
        //     'message' => 'List Data Barang',
        //     'data'    => $barang  
        // ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi data
        $validated = $request->validate([
            'supplier' => 'required',
            'nama' => 'required',
            'cover' => 'required|image|max:2048',
            'stok' => 'required|numeric'
        ]);

        $barang = new Barang;
        $barang->supplier = $request->supplier;
        $barang->nama = $request->nama;
        // upload image / foto
        if ($request->hasFile('cover')) {
            $image = $request->file('cover');
            $name = rand(1000, 9999) . $image->getClientOriginalName();
            $image->move('image/book/', $name);
            $barang->cover = $name;
        }
        $barang->stok = $request->stok;
        $barang->save();
        
        return redirect()->route('barang.index');

        
        // return response()->json([
        //     'success' => true,
        //     'message' => 'List Data Barang',
        //     'data'    => $barang  
        // ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $barang = Barang::findOrFail($id);
         return view('barang.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $barang = Barang::findOrFail($id);
         return view('barang.edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // validasi data
         $validated = $request->validate([
            'supplier' => 'required',
            'nama' => 'required',
            'stok' => 'required',
        ]);

        $brang = Barang::findOrFail($id);
        $barang->supplier = $request->supplier;
        $barang->nama = $request->nama;
        $barang->stok = $request->stok;
        $barang->save();
        return redirect()->route('barang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::findOrFail($id);
        $barang->delete();
        return redirect()->route('barang.index');
    }
}
