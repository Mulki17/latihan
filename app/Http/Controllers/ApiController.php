<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function index(){

         $barang = Barang::All();
        //Ubah ke Json
        return response()->json([
            'success' => true,
            'message' => 'List Data Barang',
            'data'    => $barang  
        ], 200);
    }

    public function create(){
        // 
    }

    public function store(Request $request){
       // validasi data
       $validated = $request->validate([
        'nama' => 'required',
        'jumlah' => 'required'
    ]);

    $barang = new Barang;
    $barang->nama = $request->nama;
    $barang->jumlah = $request->jumlah;
    $barang->save();
    
    //Ubah ke Json
    return response()->json([
        'success' => true,
        'message' => 'List Data Barang',
        'data'    => $barang  
    ], 200);
    
    }


    public function show($id){

        $barang = Barang::find($id);
        //Ubah ke Json
        return response()->json([
            'success' => true,
            'message' => 'List Data Barang',
            'data'    => $barang  
        ], 200);
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id){

        $barang = Barang::findOrFail($id);
        $barang->nama = $request->nama;
        $barang->jumlah = $request->jumlah;
        $barang->save();

        return response()->json([
            'success' => true,
            'message' => 'List Ubah Data Barang',
            'data'    => $barang  
        ], 200);
    }
    



    // public function update(Request $request, $id)
    // {

    //     $barang = Barang::findOrFail($id);
    //     $barang->nama = $request->nama;
    //     $barang->jumlah = $request->jumlah;
    //     $barang->save();

    //     return response()->json([
    //         'success' => true,
    //         'message' => 'Data Barang Berhasil diedit',
    //         'data' => $barang,
    //     ], 200);
    // }
    


    // public function update(Request $request, $id)
    // {
    //     $barang = Barang::findOrFail($id);
    //     $barang->nama = $request->nama;
    //     $barang->jumlah = $request->jumlah;
    //     $barang->save();
    //     return response()->json([
    //         'success' => true,
    //         'message' => 'Data Barang Berhasil diedit',
    //         'data' => $barang,
    //     ], 200);

    // }



    
}
