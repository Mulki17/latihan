<?php

namespace App\Models;
use Alert;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;
    protected $fillable = ['nama','jumlah'];
    public $timestamps = true;

    public function barang_masuk()
    {
        // data model "Author" bisa memiliki banyak data
        // dari model "Book" melalui fk "author_id"
        $this->hasMany('App\Models\barang_masuk', 'id_barang');
    }
    // UPLOAD IMAGE
    public function image()
    {
        if ($this->cover && file_exists(public_path('image/book/' . $this->cover))) {
            return asset('image/book/' . $this->cover);
        } else {
            return asset('image/no_image.png');
        }
    }

    public function deleteImage()
    {
        if ($this->cover && file_exists(public_path('image/book/' . $this->cover))) {
            return unlink(public_path('image/book/' . $this->cover));
        }

    }
    // public static function boot()
    // {
    //     parent::boot();
    //     self::deleting(function ($barang) {
    //         if ($barang->barang_masuk->count() > 0) {
    //             Alert::error('Failed', 'Data not deleted');
    //             return false;
    //         }
    //     });
    // }
}
